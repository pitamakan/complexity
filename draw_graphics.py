import matplotlib.pyplot as plt
import numpy as np
import sys

f = open(sys.argv[1], "r")
approx = []
while True:
    a = f.readline()
    if a == "":
        break
    a, b, c = a.split()
    a = int(a)
    b = int(b)
    c = int(c)
    approx.append([b / a, c / a])
f.close()
x = np.array(sorted(approx, key=lambda a_entry: a_entry[1]))
x = x.T

plt.figure(figsize=(15, 9))
plt.title("Отклонение ответа от лучшего")
plt.ylabel("Отношение длины ответа к длине лучшего ответа")
for i in [0, 1]:
    plt.ylim(bottom=0.99, top=2.01)
    plt.plot(np.arange(len(x[i])), x[i], label="Жадный алгоритм" if i == 0 else "Алгоритм 4-приближения")
    plt.legend(fontsize=20, loc=0)
plt.grid()
plt.savefig("graphic.png")

fig, ax = plt.subplots(1, 2, figsize=(10, 5))
fig.suptitle("Частота появления отклонений алгоритма от лучшего ответа")
for i in [0, 1]:
    a = np.array(x[i])
    ax[i].set_title("Жадный алгоритм" if i == 0 else "Алгоритм 4-приближения")
    ax[i].set_xlabel("В " + str((a != 1.0).sum() * 100 / len(a))[:4] + "% случаев ответ хуже")
    ax[i].set_ylabel("Частота отношения")
    ax[i].set_ylim(ymin=0, ymax=20)
    a = a[a != 1.0]
    ax[i].hist(x=a, bins=50, density=True)
fig.savefig("graphic1.png")


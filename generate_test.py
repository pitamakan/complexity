import random

test_count = 10**4

def GetString():
    str_len = random.randint(6, 14)
    new_str = ""
    for i in range(str_len):
        p = random.randint(0, 2)
        new_str += "a" if p else "b"
    return new_str


for i in range(test_count):
    sample_size = random.randint(4, 10)
    arr = list()
    for i in range(sample_size):
        arr.append(GetString())
    print(sample_size)
    print(*arr)
    print()
    
special_test_count = 10**3

for i in range(special_test_count):
    a = "a" + "b"*i
    b = "b"*i + "c"
    c = "b"*(i+1)
    print(3)
    print(a, b, c)
    print()